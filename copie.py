# -*- coding: utf-8 -*-
"""Module to copy files from one dir to another"""
import os
import time
import random
import shutil
import errno
import sys
import re
import math

from hardware.hardware import *
from logger import *
from settings import *
# doc
# Nb d'octets max à copier, pathFrom, pathTo, pathReceive, Num de l'échange
# usb2DDR = transfert(1000000,'/home/samuel/junk','/media/samuel/SANS TITRE', '/home/samuel/receive',2)
# usb2DDR.copyLoop()


def megatobit(size):
    return size*1E6

class Transfert(object):

    """Hold all about copy"""
    def __init__(self, pathFrom, pathTo, pathReceive, nbClient,SIZE_MAX):
        """Summary
        Args:
            pathFrom (string): path de l'upload (source du transfer)
            pathTo (string): path du download (cible du transfer)
            nbClient (int): numéro de la transaction (compteur de nombre de transfert)
        """
        
        self.iniTime = time.clock()
        self.nbClient = nbClient
        self.pathTo = os.path.abspath(pathTo) # + '/' + str(nbClient) + '/'
        self.pathFrom = os.path.abspath(pathFrom)
        self.pathReceive = os.path.abspath(pathReceive)
        if (self.pathTo == self.pathReceive):
            self.pathUsb = self.pathFrom
        else:
            self.pathUsb = self.pathTo
        self.extens = [] # list of allowed extensions    
        extensFile = open("./Extensions.txt", 'r')
        patext = r'(\ )*\.[0-z]+' #regular expresion of an extension      
        for line in extensFile:
            m=re.match(patext, line)
            if (m):
                self.extens.append(m.group(0))
        
        self.files = []
        self.sizemin = 0
        st = os.statvfs(pathTo)
        self.freeSpace = st.f_bavail * st.f_frsize
        self.totalSize = self.getFiles()
        self.sizemax = min(self.totalSize, megatobit(SIZE_MAX), self.freeSpace)
        self.sizemax0 = self.sizemax
        #self.sizemax = 20000000
	print ("Max size ", megatobit(SIZE_MAX), self.sizemax, self.totalSize, self.freeSpace)       
         
    def getFiles(self):
        """Explore récursivement le dossier donné
        et remplis une liste des fichiers qu'il contient
        avec le chemin, le nom, et le poids du fichier, retourne le poids total
        du dossier exploré
        """
        total_size = 0
        seen = {}
        compteur = 0
        t0 = time.time()
        t1 = t0
        for dirpath, dirnames, filenames in os.walk(self.pathFrom):
            for f in filenames:
                #if (total_size >= megatobit(SIZE_MAX)):
                #    return total_size;
                fp = os.path.join(dirpath, f)
                extension = os.path.splitext(f)[1]
                #if time.time()-t0 > 20:
                #    return total_size
                if time.time()-t1 > 1:
                    t1 = time.time()
                    print ("Number of explored files ", compteur)
                if f[0]!='.' and f[0]!='$' and extension in self.extens:
                    compteur += 1
                    #if compteur%1000 == 0 :
                    #    print("Number of explored files ", compteur) 
                    try:
                        stat = os.stat(fp)
                    except OSError:
                        continue

                    try:
                        seen[stat.st_ino]
                    except KeyError:
                        seen[stat.st_ino] = True
                    else:
                        continue

                    total_size += stat.st_size
                    self.sizemin = min(self.sizemin, stat.st_size)
                    self.files.append([dirpath, f, stat.st_size])
        return total_size

    @staticmethod
    def copyObj(scs, dst):
        """copie un fichier ou dossier de src vers dst
        crée l'arborescence nécessaire
        Args:
            scs (string): path de la source
            dst (string): path de la cible
        Returns:
            void:
        """
        #try:
        #    shutil.copytree(scs, dst)
        #except OSError as exc:
        #if exc.errno == errno.ENOTDIR:
        if (not os.path.exists(dst)):
            os.makedirs(dst)
            scs_name0 = os.path.basename(scs)
            scs_name = scs_name0
            if (os.path.exists(dst + scs_name)):
                uniq = 1
                while os.path.exists(dst + scs_name):
                    num = '(%s)' % uniq
                    scs_name = os.path.splitext(scs_name0)[0] + num + os.path.splitext(scs_name0)[1]
                    uniq += 1
                shutil.copy(scs,dst+scs_name)
        else:
            try:
                shutil.copy(scs, dst)
            except:
                print("File ",scs, " could not be found or copied")    
            #else:
            #    raise

    def copyRand(self):
        """Summary
           copie d'un fichier ou dossier de la liste (variable globale)
           choisi au hasard de la clé vers le DDRPath
           supprime ce fichier de la liste, retranche son poids à sizemax
        Returns:
            void: Description
        """
	print "copyRand"
        nbFiles = (len(self.files))
        if (nbFiles<1):
            return 0
        if (nbFiles>2):
            fileNb = random.randrange(0, nbFiles-1)
        else:
            fileNb = 0
        file_copied = 0
        print('index of file ', fileNb, ' size ', self.files[fileNb][2], ' max size ', self. sizemax, ' nb files ', nbFiles )
        if (self.files[fileNb][2] <= self.sizemax):
            
            chem = self.files[fileNb][0]
            fich = self.files[fileNb][1]
            if ( chem[-1:] == '/'):
                self.copyObj((self.files[fileNb][0] + self.files[fileNb][1]), self.pathTo)
            else:
                self.copyObj((self.files[fileNb][0] + '/' + self.files[fileNb][1]), self.pathTo)
            file_copied =  self.files[fileNb][1]
            self.sizemax = self.sizemax - self.files[fileNb][2]
        self.files.pop(fileNb)
        return file_copied

    # Test si le contenu de la clé peut entrer entièrement
    # si il ne peut pas, on copie des fichiers au hasard
    # on s'arrête quand sizemax est assez petit
    def copyLoop(self, fclient, fadmin):
        """Copy Files"""
        print "copyLoop"
        nbFiles = 0
        nbCopy = 0
        ledState = 0
        sizeMax0 = self.sizemax
        random.seed()
        if self.sizemin < self.sizemax:
            nbFiles = len(self.files)
            print (SIZE_RATIO,sizeMax0,SIZE_RATIO*sizeMax0,self.sizemax,NB_FILES+1)
            while self.sizemax > max(self.sizemin,1000) and nbFiles > 0 and NB_FILES>nbCopy:
                tsizemax = self.sizemax
                file_copied = self.copyRand()
                if ledState != math.floor(5*(1.-float(self.sizemax)/sizeMax0)):
                    ledState = max(math.floor(5*(1.-float(self.sizemax)/sizeMax0)),5*nbCopy/NB_FILES)
                    changeLedStripState(ledState)
                    print("Level of led ", ledState, " max ", self.sizemax, " max0 ", sizeMax0)
                if (tsizemax != self.sizemax):
                    writeSingleCopy(fclient, fadmin, file_copied)
                    nbCopy = nbCopy + 1
                nbFiles = len(self.files)

            return nbCopy

