# -*- coding: utf-8 -*-

# This file contains the different functions
# to access (read/write) the hardware (leds, pot and servo)

# import system modules
import time
import os
import RPi.GPIO as GPIO

from wiring import *
from printer import ThermalPrinter
# IO functions : initHardware should be called at
# the beginning of the program

# Read and return pot value
# Output : int 0 - 1023


def readPot():
    # check that channel is between 0 and 7
    if ((CHANNEL_PIN > 7) or (CHANNEL_PIN < 0)):
        return -1
    GPIO.output(SPICS_PIN, True)
    GPIO.output(SPICLK_PIN, False)  # start clock low
    GPIO.output(SPICS_PIN, False)        # bring CS low
    commandout = CHANNEL_PIN
    commandout |= 0x18  # start bit + single-ended bit
    commandout <<= 3        # we only need to send 5 bits here
    for i in range(5):
        if (commandout & 0x80):
            GPIO.output(SPIMOSI_PIN, True)
        else:
            GPIO.output(SPIMOSI_PIN, False)
        commandout <<= 1
        GPIO.output(SPICLK_PIN, True)
        GPIO.output(SPICLK_PIN, False)
    adcout = 0
    # read in one empty bit, one null bit and 10 ADC bits
    for i in range(12):
        GPIO.output(SPICLK_PIN, True)
        GPIO.output(SPICLK_PIN, False)
        adcout <<= 1
        if (GPIO.input(SPIMISO_PIN)):
            adcout |= 0x1
    GPIO.output(SPICS_PIN, True)
    adcout /= 2        # first bit is 'null' so drop it
    return adcout

# Switch on Led
# Input : LED_PIN : led BCM pin number


def onLed(LED_PIN):
    GPIO.output(LED_PIN, True)
# Switch off led
# Input : LED_PIN : led  BCM pin number


def offLed(LED_PIN):
    GPIO.output(LED_PIN, False)

# Make led blink
# Input: LED_PIN BCM : led pin number
#        delay       : time between blink in seconds
# Should be non blocking !


def blinkLed(LED_PIN, delay=1):
    onLed(LED_PIN)
    time.sleep(delay)
    offLed(LED_PIN)
    time.sleep(delay)


# Functions for main program
def changeLedStripState(state):
    if state == 1:
        onLed(PROGRESSA_LED_PIN)
        offLed(PROGRESSB_LED_PIN)
        offLed(PROGRESSC_LED_PIN)
        offLed(PROGRESSD_LED_PIN)
    elif state == 2:
        onLed(PROGRESSA_LED_PIN)
        onLed(PROGRESSB_LED_PIN)
        offLed(PROGRESSC_LED_PIN)
        offLed(PROGRESSD_LED_PIN)
    elif state == 3:
        onLed(PROGRESSA_LED_PIN)
        onLed(PROGRESSB_LED_PIN)
        onLed(PROGRESSC_LED_PIN)
        offLed(PROGRESSD_LED_PIN)
    elif state == 4:
        onLed(PROGRESSA_LED_PIN)
        onLed(PROGRESSB_LED_PIN)
        onLed(PROGRESSC_LED_PIN)
        onLed(PROGRESSD_LED_PIN)
    elif state == 0:
        # Turn off led strip
        offLed(PROGRESSA_LED_PIN)
        offLed(PROGRESSB_LED_PIN)
        offLed(PROGRESSC_LED_PIN)
        offLed(PROGRESSD_LED_PIN)


def powerDetectionLed():
    onLed(DETECT_LED_PIN)


def stopDetectionLed():
    offLed(DETECT_LED_PIN)


def powerEndLed():
    onLed(END_LED_PIN)


def stopEndLed():
    offLed(END_LED_PIN)

def warn_user():
    t = 0
    while t < 20:
        time.sleep(0.2)
        changeLedStripState(0)
        time.sleep(0.2)
        changeLedStripState(4)
        t+= 1

    changeLedStripState(0)

def initHardware():
    # Init layout
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # Set outputs
    GPIO.setup(SPIMOSI_PIN, GPIO.OUT)
    GPIO.setup(SPIMISO_PIN, GPIO.IN)
    GPIO.setup(SPICLK_PIN, GPIO.OUT)
    GPIO.setup(SPICS_PIN, GPIO.OUT)
    GPIO.setup(DETECT_LED_PIN, GPIO.OUT)
    GPIO.setup(PROGRESSA_LED_PIN, GPIO.OUT)
    GPIO.setup(PROGRESSB_LED_PIN, GPIO.OUT)
    GPIO.setup(PROGRESSC_LED_PIN, GPIO.OUT)
    GPIO.setup(PROGRESSD_LED_PIN, GPIO.OUT)
    GPIO.setup(END_LED_PIN, GPIO.OUT)

    # Init leds :
    changeLedStripState(0)
    powerDetectionLed()
    stopEndLed()

    t = 0
    tmax = 0
    while t < tmax:
        for i in range(5):
            changeLedStripState(i)
            time.sleep(0.25)
        t += 1

    # Init printer 
    printer = ThermalPrinter()

    changeLedStripState(0)
    stopDetectionLed()
    powerEndLed()

    return printer

def stopHardware():
    changeLedStripState(0)
    stopEndLed()
    stopDetectionLed()
