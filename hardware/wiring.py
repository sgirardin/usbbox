# -*- coding: utf-8 -*-
"""Summary"""

# This file contains the pin wiring
# of the RPI

# leds
DETECT_LED_PIN = 21
PROGRESSA_LED_PIN = 4
PROGRESSB_LED_PIN = 17
PROGRESSC_LED_PIN = 27
PROGRESSD_LED_PIN = 22
END_LED_PIN = 5

# potentiometer
SPICLK_PIN = 18
SPIMISO_PIN = 23
SPIMOSI_PIN = 24
SPICS_PIN = 25
CHANNEL_PIN = 0

# servo
SERVO_PIN = 0



