# -*- coding: utf-8 -*-
"""Main program for PC"""
import time
import os

from copie import Transfert
from logger import *
from hardware.wiring import *


def listDevices():
    """Summary
    Returns:
        TYPE: Description
    """
    # list of usb devices
    list_ = []
    partitionsFile = open("/proc/partitions")
    lines = partitionsFile.readlines()[2:]  # Skips the header lines

    for line in lines:
        words = [x.strip() for x in line.split()]
        minorNumber = int(words[1])
        deviceName = words[3]
        if minorNumber % 16 == 0:
            path = "/sys/class/block/" + deviceName
            if os.path.islink(path):
                if os.path.realpath(path).find("/usb") > 0:
                    name = "/dev/%s" % deviceName
                    list_.append(name)

    return list_


def detectNewDevice(PLUGGED):
    """Summary
    Args:
        PLUGGED (TYPE): Description
    Returns:
        TYPE: Description
    """
    new = False
    while not new:
        devices = listDevices()
        print(len(devices))
        if len(devices) > PLUGGED:
            new = True
    # new device detected
    print("New device : %s" % devices[-1])
    return devices[-1]


def mountDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    # name = name + "1" # partition

    # if (os.path.exists(TMPMOUNTDIR)==0):
    # 
    # os.system(command)

    command = "sudo mount -t auto " + name + " " + MOUNT_POINT + " -o uid=1000"
    try:
        os.system(command)
        # os.system("sudo chmod 777 " + MOUNT_POINT)
        print("USB device %s mounted successfully in %s!" % (name, MOUNT_POINT))
        return True
    except:
        print("Error: USB device %s not mounted in %s!" % (name, MOUNT_POINT))
        # signal error by making led blinking
        # until key is removed (e.g. number of devices go down to PLUGGED
        # number)
        while len(listDevices()) != PLUGGED:
            pass
            # blinkLed(DETECT_LED_PIN)
        return False

def ejectDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    command = "sudo umount " + MOUNT_POINT  # + "1" # unmount partition
    # command = "udisks --unmount " + name  #+ "1"

    try:
        os.system(command)
        print( "USB device ready to be ejected !")
    except:
        print ("Error unmounting device")

    command = "udisks --eject " + name
    try:
        os.system(command)
        print ("USB device ejected!")
    except:
        print ("Error ejecting device")

def main():

    # Initialisation of the program
    # Creation of the log
    fadmin = createAdminLog()
    PLUGGED = len(listDevices())

    while 1:
        
        # Init of system
         initHardware()
        
        # get number of usb devices
        print( "%d devices already PLUGGED !" % PLUGGED)

        # listen for new devices
        devname = detectNewDevice(PLUGGED)

        #if we arrived here : new device detected
         powerDetectionLed()
         stopEndLed()

        # mount usb device
        if devname:
            print ("Mounted: %s" % devname)
            mountDevice(devname)

            # get time setting between 0 et SIZE_MAX en Mo
            space_allowed = readPot()
            print ("time_allowed", space_allowed)
            
            # init client log
            fclient = createClientLog(fadmin)

            # Sending step
            sending = Transfert(DD_SEND, MOUNT_POINT, DD_RECV, space_allowed, 2)
            files_count = sending.copyLoop(fclient, fadmin)
            endClientLog(fclient, files_count)
            startAdminRecv(fadmin, files_count)
            
            # Receiving step
            receiving = Transfert(MOUNT_POINT, DD_SEND, DD_RECV, space_allowed, 2)
            files_count = receiving.copyLoop(fclient, fadmin)
            endAdminRecv(fadmin, files_count)

            # unmount and eject usb device
            ejectDevice(devname)
            print  ("Ejected: %s" % devname)
        else:
            pass


    # close log file
    endAdminLog(fadmin)  


if __name__ == '__main__':

    main()
