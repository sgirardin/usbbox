# -*- coding: utf-8 -*-
"""Functions to write different types of logs"""
import time
import sys
import shutil
import linecache
import random

from os.path import join
from datetime import datetime

from settings import *

ADMINLOG_DATE_START = "Start at %s\n"
ADMINLOG_DATE_END = "End at %s\n"
ADMINLOG_START_SEND = "-> Copie sur clé USB\n"
ADMINLOG_START_RECV = "-> Réception de clé USB\n"
LINE = "*******************************\n"

def printErr (txt):
    inf = sys.exc_info()
    print (time.strftime("\n%A %d %B %Y %H:%M:%S"))
    print (txt, inf[0])
    for i in inf:
        print i
    

def writeonBoth(f, printer, txt):
    print "writeonBoth\n"
    # write in log file
    f.write(txt)
    # write with printer
    printer.print_text(txt)
    printer.linefeed()
    time.sleep(1)

def createAdminLog():
    """ Create admin log file at init """
    print "create admin log"
    filename = join(ADMINLOG_LOCATION, ADMINLOG_FILENAME)
    date = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    f = open(filename, 'a')
    f.write(ADMINLOG_HEADER + '\n')
    f.write(ADMINLOG_DATE_START % date + '\n')

    # init data ..?

    return f

def createClientLog(fadmin, printer,location):
    """ Create client log file at init """
    #filename = join(location, USBLOG_FILENAME)
    print "createClientLog"
    filename = join(location, USBLOG_FILENAME)
    #cmd = 'sudo rm '+TMP_LOG
    #print (cmd)
    #print "command system..."
    #os.system(cmd)
    #print "... done !"
    print ("remove "+TMP_LOG+"...")
    try:
        shutil.rmtree(TMP_LOG)
    except:
        pass
        #printErr ("Erreur : impossible d'effacer le fichier "+TMP_LOG+" : ")
    time.sleep(1)

    try:
        import os.path
        if os.path.isfile(filename):
            #cmd = 'sudo mv '+filename+' '+TMP_LOG
            #print (cmd)
            print ("mv "+filename+" "+TMP_LOG)
            shutil.move(filename, TMP_LOG)
            time.sleep(2)
    except:
        pass
    
    filename = TMP_LOG
    
    print("Emplacement du log : ", filename)
    
    date = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    try :
        f = open(filename, 'a')
    except :
        print("Impossible d'ouvrir le log ", filename)

    print ("log = ", f)
    
    # f.write(USBLOG_HEADER + '\n')
    # f.write(USBLOG_INFOS + '\n')
    # f.write(date + '\n')
    # f.write('\n')
    # f.write(LINE)

    f.write('\n')
    line = linecache.getline("phrase.txt",random.randint(0,NB_SENTENCE))
    f.write(line)
    line = linecache.getline("phrase.txt",random.randint(0,NB_SENTENCE))
    f.write(line)
    line = linecache.getline("phrase.txt",random.randint(0,NB_SENTENCE))
    f.write(line)
    f.write('\n')
    writeonBoth(f, printer, USBLOG_HEADER)
    writeonBoth(f, printer, USBLOG_INFOS)
    writeonBoth(f, printer, date)
    f.write('\n')
    printer.linefeed()
    writeonBoth(f, printer, LINE)

    # write header on admin log
    startAdminSend(fadmin, date)
    for i in range(3):
        printer.linefeed()

    return f

def endClientLog(f, files_count, location):
    """ End client log file at init """
    print "endClientLog"
    f.write("%s fichiers copiés\n" % files_count)
    f.write(LINE)
    try :
        f.close()
    except :
        print("Fichier non fermé")
    try:
        filename = join(location, USBLOG_FILENAME)
        #cmd = 'sudo mv '+TMP_LOG+' '+filename
        #print (cmd)
        #os.system(cmd)
        print ("mv "+TMP_LOG+" "+filename)
        shutil.move(TMP_LOG, filename)
        time.sleep(2)
    except:
        printErr("Error during the copy of log file : ")

    
def writeSingleCopy(f, fadmin, file_copied):
    f.write("Copy of %s\n" % file_copied)
    fadmin.write("Copy of %s\n" % file_copied)

def startAdminSend(f, date):
    print "startAdminSend"
    f.write(date + '\n')
    f.write(LINE)
    f.write(ADMINLOG_START_SEND)

def endCopy(f, file_copied):
    print "endCopy"
    f.write("(%s fichiers copiés)\n" % file_copied)

def startAdminRecv(f, file_copied):
    print "startAdminRecv"
    endCopy(f, file_copied)
    f.write('\n')
    f.write(ADMINLOG_START_RECV)

def endAdminRecv(f, file_copied):
    print "endAdminRecv"
    endCopy(f, file_copied)
    f.write(LINE)
    f.write('\n')

def endAdminLog(f):
    # save data
    print "endAdminLog"
    # stop or close sthg ?
    date = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

    # write end time
    f.write(ADMINLOG_DATE_END % date)
    f.close()
