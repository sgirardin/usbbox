import subprocess
import os
import time
import random
import shutil, errno

NbClient = 21
sizemax = 2000
sizemin = sizemax
usbPath = './testDossier' # path de la clé
DDRPath = './FromUSB/'+str(NbClient)+'/' # path du disque dur
liste = [] # initialisation de la liste des fichiers

st = os.statvfs(usbPath)
free = st.f_bavail * st.f_frsize
print("Espace libre : ",free)

#Explore récursivement le dossier donné
#et remplis une liste des fichiers qu'il contient
#avec le chemin, le nom, et le poids du fichier, retourne le poids total du dossier exploré
t1 = time.clock()
def get_size2(start_path='.'):
    total_size = 0
    seen = {}
    global liste,sizemin
    compteur = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            compteur += 1
            fp = os.path.join(dirpath, f)
            try:
                stat = os.stat(fp)
            except OSError:
                continue
            
            try:
                seen[stat.st_ino]
            except KeyError:
                seen[stat.st_ino] = True
            else:
                continue
            
            total_size += stat.st_size
            sizemin = min(sizemin,stat.st_size)
            liste.append([dirpath,f,stat.st_size])
    #    print(dirpath,f)

    return total_size

#copie un fichier ou dossier de src vers dst
#crée l'arborescence nécessaire
def copyfromto(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            if (not os.path.exists(dst)):
                os.makedirs(dst)
            shutil.copy2(src, dst)
        elif exc.errno == errno.EEXIST:
            print("ERREUR : On tente de copier ", src, "vers ", dst, "qui existe déjà")
            print("Normalement, on crée un dossier de numéro différent du précédent")
        else: raise

SizeKb = get_size2(usbPath) # remplissage de la liste et taille sur la clé


# copie d'un fichier ou dossier de la liste (variable globale)
# choisi au hasard de la clé vers le DDRPath
# supprime ce fichier de la liste, retranche son poids à sizemax
def CopyFile():
    global sizemax
    NbFiles = (len(liste))
    random.seed()
    FileNb = random.randrange(0,NbFiles)
    if (liste[FileNb][2]<sizemax):
        copyfromto(liste[FileNb][0]+'/'+liste[FileNb][1],DDRPath)
        sizemax = sizemax - liste[FileNb][2]
    liste.pop(FileNb)





# Test si le contenu de la clé peut entrer entièrement
# si il ne peut pas, on copie des fichiers au hasard
# on s'arrête quand sizemax est assez petit
if (sizemin<sizemax):
    if (SizeKb < sizemax):
        copyfromto(usbPath,DDRPath)
    else:
        NbFiles = (len(liste))
        while (sizemax > sizemin and NbFiles > 0):
            CopyFile()
            NbFiles = (len(liste))
    


print("Temps d'execution : ", time.clock()-t1)



