import subprocess
import os
import time
import random

rep_cour = os.getcwd()
print(rep_cour)

#donne la taille en octet
print(os.path.getsize('/Projets/usbbox/tests/led.py'))

print(os.curdir)

t1 = time.clock()
def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

print(get_size('./testDossier/'),time.clock()-t1)

t1 = time.clock()
def du(path):
    """disk usage in human readable format (e.g. '2,1GB')"""
    return subprocess.check_output(['du','-sh', path]).split()[0].decode()

print(du('./testDossier/'),time.clock()-t1)

t1 = time.clock()
def getFolderSize(folder):
    total_size = os.path.getsize(folder)
    for item in os.listdir(folder):
        itempath = os.path.join(folder, item)
        if os.path.isfile(itempath):
            total_size += os.path.getsize(itempath)
        elif os.path.isdir(itempath):
            total_size += getFolderSize(itempath)
    return total_size

print("Size: " + str(getFolderSize("./testDossier/")),time.clock()-t1)


#rapide et efficace
t1 = time.clock()
def get_size2(start_path='.'):
    total_size = 0
    seen = {}
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            try:
                stat = os.stat(fp)
            except OSError:
                continue

            try:
                seen[stat.st_ino]
            except KeyError:
                seen[stat.st_ino] = True
            else:
                continue

            total_size += stat.st_size

    return total_size

print(get_size2('./testDossier'),time.clock()-t1)

#plus rapide mais un peu faux
t1 = time.clock()
def getFolderSize2(p):
   from functools import partial
   prepend = partial(os.path.join, p)
   return sum([(os.path.getsize(f) if os.path.isfile(f) else getFolderSize(f)) for f in map(prepend, os.listdir(p))])
print("Size: " + str(getFolderSize("./testDossier/")),time.clock()-t1)  













