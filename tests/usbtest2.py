import usb.core
import usb.backend.libusb1

def getUSBdevices():
    list = []
    busses = usb.busses()
    for bus in busses:
        devices = bus.devices
        for dev in devices:
            if dev != None:
                try:
                    xdev = usb.core.find(idVendor=dev.idVendor, idProduct=dev.idProduct)
                    if xdev._manufacturer is None:
                        xdev._manufacturer = usb.util.get_string(xdev, xdev.iManufacturer)
                    if xdev._product is None:
                        xdev._product = usb.util.get_string(xdev, xdev.iProduct)
                    stx = '%6d %6d: '+str(xdev._manufacturer).strip()+' = '+str(xdev._product).strip()
                    # print stx % (dev.idVendor,dev.idProduct)
                    list.append(dev)
                except:
                    pass
    return list


def detectNewDevice(plugged=0):
    new = False
    while not new:
        devices = getUSBdevices()
        if len(devices) > plugged:
            new = True
    # the new device has been append at the beginning of the list
    print "New device : %s" % devices[0].idVendor
    return devices[0]


# test
list = getUSBdevices()
for dev in list:
   print dev.filename

print "Waiting for new usb key ..."
detectNewDevice(5)
