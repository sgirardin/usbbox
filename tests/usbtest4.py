# -*- coding: utf-8 -*-

import os
import sys

def listDevices():
    # list of usb devices
    list_ = []
    partitionsFile = open("/proc/partitions")
    lines = partitionsFile.readlines()[2:]#Skips the header lines
    
    for line in lines:
        words = [x.strip() for x in line.split()]
        minorNumber = int(words[1])
        deviceName = words[3]
        if minorNumber % 16 == 0:
            path = "/sys/class/block/" + deviceName
            if os.path.islink(path):
                if os.path.realpath(path).find("/usb") > 0:
                    name = "/dev/%s" % deviceName
                    list_.append(name)

    return list_         


# list devices
print listDevices()
# # usb mount temp dir
# tmpmountdir = "/home/pi/mountdir"

# # new device 
# dev = "/dev/sde1"
# command = "umount " + dev 
# os.system(command)
# if (os.path.exists(tmpmountdir)==0):
#     command = "mkdir -m 777 " + tmpmountdir
#     os.system(command)
# command = "mount -t auto " + dev + " " + tmpmountdir
# ret = os.system(command)
# if ret != 0:
#     sys.exit(0)
#     print "USB device mounted successfully!"
