import RPi.GPIO as GPIO
import time

# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)

# set up the input and output
#GPIO.setup(11, GPIO.IN)
led = 21
GPIO.setup(led, GPIO.OUT)

# read input from pin 11
#input_value = GPIO.input(11)

# set output to pin 12
while 1:
   GPIO.output(led, True)
   time.sleep(1)
   GPIO.output(led, False)
   time.sleep(1)
