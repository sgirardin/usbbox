import RPi.GPIO as GPIO

# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)

# set up the input and output
GPIO.setup(11, GPIO.IN)
GPIO.setup(12, GPIO.OUT)

# read input from pin 11
input_value = GPIO.input(11)

# set output to pin 12
GPIO.output(12, GPIO.HIGH)

