from test2 import *

rejouer = True
argent = 0
while rejouer :
    nombre = -5
    somme = -1
    while nombre<0 or nombre >49 :
        try :
            nombre = input("Donne un nombre de 0 a 49 ")
            nombre = int(nombre)
        except :
            nombre = -1
    while somme<0 :
        try :
            somme = input("Mise sur ce nombre (0 pour arreter) ")
            somme = int(somme)
        except :
            somme = -1

    if somme == 0 :
        rejouer = False
        break
    somme = int(somme)

    argent -= somme

    resultat = randrange(50)

    if nombre == resultat :
        argent += int(3*somme)
        print("Vous avez gagne. Vous possedez ", argent)
    elif nombre%2 == resultat%2 and resultat != 0 :
        argent += int(1.5*somme)
        print("Bonne couleur. Vous possedez ", argent)
    else :
        print("Vous avez perdu. Retentez votre chance. Vous possedez ", argent)