

class Humain :
    """ docstring :
    ce que fait la classe """
    nbr_humains = 0
    def __init__(self,nom = "Dupont",prenom="Jean"): # constructeur, le nom __init__ est imposé
        Humain.nbr_humains += 1
        self.nom = nom
        self.prenom = prenom
        self.age = 23
        self._lieu = "Marseille"
        self.raconte = ""
    def _get_lieu(self):
        return self._lieu
    def _set_lieu(self,nouveau_lieu):
        print("{} déménage de {} vers {}".format(self.prenom,self.lieu,nouveau_lieu))
        self._lieu = nouveau_lieu
    lieu = property(_get_lieu,_set_lieu)
    def raconter(self,texte):
        if self.raconte != "":
            self.raconte += "\n"
        self.raconte += texte
    def oublier(self):
        self.raconte = ""
    def dire(self):
        print(self.raconte)
    def combien(cls):
        print("Il y a {} humains".format(
            cls.nbr_humains))
    combien = classmethod(combien)     
bernard = Humain("Mercat")
print(bernard.nom,bernard.prenom,Humain.nbr_humains)
robert = Humain("Toromanoff","Benoit")
robert.age = 0
robert.lieu = "Lille"
robert.raconter("Agagueugueu")
robert.raconter("mamapapamumum")
print(robert.nom,robert.prenom,Humain.nbr_humains)
robert.dire()
robert.combien()