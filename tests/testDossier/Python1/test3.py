import os
import pickle

liste1 = [1,2,3,4]
liste1.append(56)
liste1.insert(1,'un')
liste1.insert(3,'deux')
print(liste1)

liste1 += [1,2,3]
print(liste1)
del liste1[1]
print(liste1)
liste1.remove(2)
print(liste1)

for i, elt in enumerate(liste1):
    print(elt)

os.chdir("/Projets/Python1")
fichier = open("ftest.txt","wb") #ecrase

mpickler = pickle.Pickler(fichier)
mpickler.dump({1,2})

fichier.close()
