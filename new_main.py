# -*- coding: utf-8 -*-
"""Main program for PC"""
import time
import os
import sys

from hardware.hardware import *
from copie import Transfert
from logger import *


def listDevices():
    """Summary
    Returns:
        TYPE: Description
    """
    # list of usb devices
    list_ = []
    partitionsFile = open("/proc/partitions")
    lines = partitionsFile.readlines()[2:]  # Skips the header lines

    for line in lines:
        words = [x.strip() for x in line.split()]
        minorNumber = int(words[1])
        deviceName = words[3]
        if minorNumber % 16 == 0:
            path = "/sys/class/block/" + deviceName
            if os.path.islink(path):
                if os.path.realpath(path).find("/usb") > 0:
                    name = "/dev/%s" % deviceName
                    list_.append(name)

    return list_


def detectNewDevice(PLUGGED):
    """Summary
    Args:
        PLUGGED (TYPE): Description
    Returns:
        TYPE: Description
    """
    print "detectNewDevice"
    new = False
    while not new:
        devices = listDevices()
        #print(len(devices))
        for d in devices:
            if d not in PLUGGED:
                new = True
                break
        #if not set(devices) == set(PLUGGED):
        #    new = True
        PLUGGED = devices
    # new device detected
    print("New device : %s" % d)
    return d

def detectNewDevice2(listDir) :
    t1 = time.time()
    while time.time()-t1<5:
        listDir2 = os.listdir(MOUNT_POINT)
        for item in listDir2 :
            if item not in listDir :
                return item
        time.sleep(.1)

def mountDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    # name = name + "1" # partition

    # if (os.path.exists(TMPMOUNTDIR)==0):
    # 
    # os.system(command)

    os.makedirs(MOUNT_POINT+USB_MOUNT_POINT)
    command = "sudo mount -t auto " + name + " " + MOUNT_POINT+USB_MOUNT_POINT + " -o uid=1000"
    print command
    try:
        os.system(command)
        # os.system("sudo chmod 777 " + MOUNT_POINT)
        print("USB device %s mounted successfully in %s!" % (name, MOUNT_POINT))
        return True
    except:
        printErr("Error: USB device %s not mounted in %s!" % (name, MOUNT_POINT))
        # signal error by making led blinking
        # until key is removed (e.g. number of devices go down to PLUGGED
        # number)
        while len(listDevices()) != PLUGGED:
            pass
            # blinkLed(DETECT_LED_PIN)
        return False

def ejectDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    print "end: device ejected"
    #print ("Should eject " + MOUNT_POINT + name + " but will not !!!" )
    #return

    try:
        shutil.rmtree(MOUNT_POINT+USB_MOUNT_POINT)
    except:
        pass
    
    command = "sudo umount " + MOUNT_POINT + name  # + "1" # unmount partition
    # command = "udisks --unmount " + name  #+ "1"

    try:
        os.system(command)
        print( "USB device unmounted")
    except:
        print ("Error unmounting device")

    command = "udisks --eject " + name
    try:
        os.system(command)
        print ("USB device ejected!//////////////////////////")
    except:
        print ("Error ejecting device")

def main():

    while 1:
        try :
            # Initialisation of the program
            # Creation of the log
            fadmin = createAdminLog()
            PLUGGED = listDevices()

            # Init of system
            printer = initHardware()
            global SIZE_MAX
            SIZE_MAX = SIZE_MIN + readPot()*SIZE_MAX0/823
            print ("Copy of "+str(SIZE_MAX)+"Mo max."+str(readPot()))
                
            # get number of usb devices
            print( "%d devices already PLUGGED !" % len(PLUGGED))
            # listen for new devices
            listDir = os.listdir(MOUNT_POINT)
            
        
            name=detectNewDevice(PLUGGED)
            if name :
                 usbname = detectNewDevice2(listDir)
            #    usbname = detectNewDevice2(listDir)
            #    if not usbname :
            #        usbname = mountDevice(name)
                    #if (usbname) :
                    #    usbname = USB_MOUNT_POINT
            #        usbname = detectNewDevice2(listDir)

            
            if usbname:
                time.sleep(7)
                print "Mounted: %s" % usbname
                #mountDevice(devname)
                powerDetectionLed()
                stopEndLed()
                # get time setting between 0 et SIZE_MAX en Mo
	        SIZE_MAX = .1 + readPot()*SIZE_MAX0/823
              
                # init client log
                fclient = createClientLog(fadmin, printer,MOUNT_POINT + usbname)

                print ("Client log : ", fclient)
                
                # changeLedStripState(2)
                # time.sleep(3)
                # changeLedStripState(3)
                # time.sleep(3)
                # changeLedStripState(4)
                # time.sleep(1)

                # Sending step
                sending = Transfert(MOUNT_POINT+usbname, DD_RECV, DD_RECV, 2,SIZE_MAX)
                files_count = sending.copyLoop(fclient, fadmin)
                startAdminRecv(fadmin, files_count)

                # # Receiving step
                receiving = Transfert(DD_SEND, MOUNT_POINT+usbname, DD_RECV, 2,SIZE_MAX)
                files_count = receiving.copyLoop(fclient, fadmin)
                endAdminRecv(fadmin, files_count)
                print ("end client log... ")
                endClientLog(fclient, files_count, MOUNT_POINT+usbname)


                # unmount and eject usb device
                ejectDevice(usbname)
                printer.linefeed()

                printer.linefeed()

                printer.linefeed()
                print  "Ejected: %s" % usbname
                stopDetectionLed()
                powerEndLed()
                printer.linefeed()
                warn_user()
                #return False
            else:
                print "Error : impossible to mount the device."

            # close log file
            endAdminLog(fadmin)  
        except :
            #raise
            printErr("Error in main : ")
            try :
                ejectDevice(usbname)
            except :
                pass
            #print("Error")

if __name__ == '__main__':

    main()
