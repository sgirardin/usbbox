# -*- coding: utf-8 -*-
"""Summary"""
import time
import os

from hardware.wiring import *
# from hardware.hardware import *
from copie import Transfert


def listDevices():
    """Summary
    Returns:
        TYPE: Description
    """
    # list of usb devices
    list_ = []
    partitionsFile = open("/proc/partitions")
    lines = partitionsFile.readlines()[2:]  # Skips the header lines

    for line in lines:
        words = [x.strip() for x in line.split()]
        minorNumber = int(words[1])
        deviceName = words[3]
        if minorNumber % 16 == 0:
            path = "/sys/class/block/" + deviceName
            if os.path.islink(path):
                if os.path.realpath(path).find("/usb") > 0:
                    name = "/dev/%s" % deviceName
                    list_.append(name)

    return list_


def detectNewDevice(PLUGGED):
    """Summary
    Args:
        PLUGGED (TYPE): Description
    Returns:
        TYPE: Description
    """
    new = False
    while not new:
        devices = listDevices()
        print(len(devices))
        if len(devices) > PLUGGED:
            new = True
    # new device detected
    print("New device : %s" % devices[-1])
    return devices[-1]


def mountDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    # name = name + "1" # partition

    # if (os.path.exists(TMPMOUNTDIR)==0):
    # command = "mkdir -m 777 " + TMPMOUNTDIR
    # os.system(command)

    command = "sudo mount -t auto " + name + " " + TMPMOUNTDIR
    try:
        os.system(command)
        print("USB device %s mounted successfully in %s!" % (name, TMPMOUNTDIR))
        return True
    except:
        print("Error: USB device %s not mounted in %s!" % (name, TMPMOUNTDIR))
        # signal error by making led blinking
        # until key is removed (e.g. number of devices go down to PLUGGED
        # number)
        while len(listDevices()) != PLUGGED:
            pass
            # blinkLed(DETECT_LED_PIN)
        return False


def ejectDevice(name):
    """Summary
    Args:
        name (TYPE): Description
    Returns:
        TYPE: Description
    """
    command = "sudo umount " + TMPMOUNTDIR  # + "1" # unmount partition
    # command = "udisks --unmount " + name  #+ "1"

    try:
        os.system(command)
        print( "USB device ready to be ejected !")
    except:
        print ("Error unmounting device")

    command = "udisks --eject " + name
    try:
        os.system(command)
        print ("USB device ejected!")
    except:
        print ("Error ejecting device")

TMPMOUNTDIR = "/home/samuel/testmount"
DDDIR = "home/samuel/junk"
PLUGGED = len(listDevices())
#
SIZE = 1000000


def main():
    """Summary

    Returns:
        TYPE: Description
    """
    while 1:
        # Init of system
        # initHardware()

        # get number of usb devices
        print( "%d devices already PLUGGED !" % PLUGGED)

        # listen for new devices
        devname = detectNewDevice(PLUGGED)

        # if we arrived here : new device detected
        # powerDetectionLed()
        # stopEndLed()
        # mount usb device
        if mountDevice(devname):
            print (devname)
            # if success
            # transfer
            # fake just for simu
            # for i in range(5):
            # 	changeLedStripState(i)
            # 	print "transfer ..."
            # 	time.sleep(2)
            # usb to dd
            usb2DDR = Transfert(SIZE, DDDIR, TMPMOUNTDIR, 2)
            usb2DDR.copyLoop()
            time.sleep(20)

            # transfer ended
            # unmount and eject usb device
            ejectDevice(devname)
            # powerEndLed()

        else:
            pass
                      
            
#from usb to receiving folder on DDR_receive
#~ usb2DDR = Transfert(100000000,'/home/jean/Projets/usbbox/truc/','/home/jean/Projets/usbbox/receive/','/home/jean/Projets/usbbox/receive/',2)
#~ usb2DDR.copyLoop()
#~ 
#~ #from DDR_send to usb
#~ usb2DDR = Transfert(1000,'/home/jean/Projets/usbbox/tests/','/home/jean/Projets/usbbox/truc/','/home/jean/Projets/usbbox/receive/',2)
#~ usb2DDR.copyLoop()


if __name__ == '__main__':
    main()
