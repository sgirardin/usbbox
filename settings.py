# -*- coding: utf-8 -*-
from hardware.hardware import *


######################################################
#			      HARDDRIVE SETTINGS  		         #
######################################################
# Chemin du disque dur qui recoit les données des clés usb
DD_RECV = "/media/pi/OUT" #/home/pi/DD_RECV"
#"/media/pi/Réservé au système/recv"
# Chemin du disque dur qui envoie les données vers les clés usb
DD_SEND = "/media/pi/HDD" #/home/pi/DD_SEND"
#"/media/pi/70000F5F000F2BA2/Documents and Settings/Simon/Downloads"
# Don't change this one
MOUNT_POINT = "/media/pi/"

USB_MOUNT_POINT = "usbMountPoint/"
USBLOG_LOCATION = MOUNT_POINT

######################################################
#			       LOG FILE SETTINGS  		         #
######################################################


# Log sur clé USB clients
USBLOG_FILENAME = "log_atada.txt"
NB_SENTENCE = 29
USBLOG_HEADER = """A TA DATA\n
Texte sur la machine [..]\n
PEKK Luka - GIRARDIN Samuel - MERCAT Jean\n\n"""
USBLOG_INFOS = "Bordeaux - CAPC\n"
USBLOG_FOOTER = ""
TMP_LOG = "/home/pi/tmp/log.txt"


# Log sur RPI
ADMINLOG_LOCATION = DD_RECV
ADMINLOG_FILENAME = "log_prog.txt"
ADMINLOG_HEADER = "Log of the program for administration task\n"
ADMINLOG_FOOTER = "End of the program log\n"

# Log sur ticket de caisse
# TICKETLOG_HEADER =
# TICKETLOG_FOOTER =


######################################################
#			        USAGE SETTINGS    		         #
######################################################
# Taille maximum totale de transfert (Mo)
SIZE_MAX0 = 2000
SIZE_MIN = 100
# Nombre maximum de fichiers à copier
NB_FILES = 20
# Ratio de la taille max des fichiers à copier
SIZE_RATIO = 0.6
#SIZE_MAX = SIZE_MAX0
